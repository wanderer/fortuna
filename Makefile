cppch = cppcheck
cppch_args = --language=c++ --std=c++20 --enable=all --verbose --suppress=unmatchedSuppression --suppress=missingIncludeSystem ./*.{cpp,h}
c = cmake
c_args = -G Ninja -DCMAKE_BUILD_TYPE=
c_args_d = $(c_args)Debug
c_args_r = $(c_args)Release
cl = -DCMAKE_CXX_COMPILER=clang++
g = -DCMAKE_CXX_COMPILER=g++
n = ninja
n_args = -C
t_folder = /tmp/.fortuna_workdir/build
d_folder = cmake-build-debug
d_folder_cl = $(d_folder)_cl
dt_folder = $(t_folder)/$(d_folder)
r_folder = cmake-build-release
rt_folder = $(t_folder)/$(r_folder)
s_folder = cmake-build-san
s_folder_cl = $(s_folder)_cl
st_folder = $(t_folder)/$(s_folder)
tidy_folder = cmake-build-tidy
tidyt_folder = $(t_folder)/$(tidy_folder)
s_on = -DSAN=ON
s_off = -DSAN=OFF
t_on = -DTIDY=ON
t_off = -DTIDY=OFF
v = valgrind
v_on = -DVALGRIND=ON
v_off = -DVALGRIND=OFF
v_db = $(d_folder)_valgr
v_rl = $(r_folder)_valgr

.PHONY: check tidy build debug debug_cl release valgrind san san_cl test clean distclean

debug:
ifneq ("${CI}","true")
	@if [ ! -L "$(d_folder)" ]; then \
		mkdir -pv $(dt_folder) && ln -sfv $(dt_folder) $(d_folder);\
	fi
else
	@if [ ! -d "$(d_folder)" ]; then mkdir -pv $(d_folder); fi
endif
	$(c) $(c_args_d) $(g) $(s_off) $(t_off) $(v_off) -B$$(readlink -f $(d_folder))
	$(n) $(n_args) $(d_folder)

debug_cl:
	if [ ! -d "$(d_folder_cl)" ]; then mkdir -pv $(d_folder_cl); fi
	$(c) $(c_args_d) $(cl) $(s_off) $(t_off) $(v_off) -B$(d_folder_cl)
	$(n) $(n_args) $(d_folder_cl)

release:
ifneq ("${CI}","true")
	@if [ ! -L "$(r_folder)" ]; then \
		mkdir -pv $(rt_folder) && ln -sfv $(rt_folder) $(r_folder);\
	fi
else
	@if [ ! -d "$(r_folder)" ]; then mkdir -pv $(r_folder); fi
endif
	$(c) $(c_args_r) $(g) $(s_off) $(t_off) $(v_off) -B$$(readlink -f $(r_folder))
	$(n) $(n_args) $(r_folder)

check:
	$(cppch) $(cppch_args)
	make tidy

san:
ifneq ("${CI}","true")
	@if [ ! -L "$(s_folder)" ]; then \
		mkdir -pv $(st_folder) && ln -sfv $(st_folder) $(s_folder);\
	fi
else
	@if [ ! -d "$(s_folder)" ]; then mkdir -pv $(s_folder); fi
endif
	$(c) $(c_args_d) $(g) $(s_on) $(t_off) $(v_off) -B$$(readlink -f $(s_folder))
	$(n) $(n_args) $(s_folder)

san_cl:
	if [ ! -d "$(s_folder_cl)" ]; then mkdir -pv $(s_folder_cl); fi
	$(c) $(c_args_d) $(cl) $(s_on) $(t_off) $(v_off) -B$(s_folder_cl)
	$(n) $(n_args) $(s_folder_cl)


tidy:
ifneq ("${CI}","true")
	if [ ! -L "$(tidy_folder)" ]; then \
		mkdir -pv $(tidyt_folder) && \
		ln -sfv $(tidyt_folder) $(tidy_folder);\
	fi
else
	@if [ ! -d "$(tidy_folder)" ]; then mkdir -pv $(tidy_folder); fi
endif
	$(c) $(c_args_d) $(g) $(s_off) $(t_on) $(v_off) -B$$(readlink -f $(tidy_folder))
	$(n) $(n_args) $(tidy_folder)

valgrind: valgrind-debug

valgrind-debug:
	if [ ! -d "$(v_db)" ]; then mkdir -pv "$(v_db)"; fi
	$(c) $(c_args_d) $(g) $(s_off) $(t_off) $(v_on) -B"$(v_db)"
	$(n) $(n_args) "$(v_db)"
	$(v) --leak-check=full ./$(v_db)/fortuna

valgrind-release:
	if [ ! -d "$(v_rl)" ]; then mkdir -pv "$(v_rl)"; fi
	$(c) $(c_args_r) $(g) $(s_off) $(t_off) $(v_on) -B"$(v_rl)"
	$(n) $(n_args) "$(v_rl)"
	$(v) ./$(v_rl)/fortuna

build: debug

test: check build

clean:
	@find . -maxdepth 5 -follow -type f -regextype posix-egrep -regex ".*\.(o|dwo|out|bin|cmake|txt)" -not -name CMakeLists.txt -not -path "./lib/*" -not -path "./.git/*" -delete

distclean:
	@find . -iwholename '*cmake*' -not -name CMakeLists.txt -not -path "./lib/*" -delete

