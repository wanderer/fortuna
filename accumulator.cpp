#ifndef FORTUNA_ACCUMULATOR_CPP
#define FORTUNA_ACCUMULATOR_CPP

#include "accumulator.h"
#include "pool.h"

#include <fmt/core.h>

#include <algorithm>
#include <array>
#include <atomic>
#include <chrono>
#include <cstdint>
#include <memory>
#include <thread>
#include <vector>

namespace fortuna {
namespace accumulator {

auto wait_for(const unsigned int& milliseconds) -> void {
	std::this_thread::sleep_for(std::chrono::milliseconds{milliseconds});
}

Accumulator::Accumulator() noexcept {
	this->Gen = nullptr;
	this->_p_pools = nullptr;
}
Accumulator::~Accumulator() noexcept {}


auto Accumulator::set_reseed_ctr_to_null() noexcept -> void {
	std::lock_guard<std::mutex> lg(mtx);
	this->reseed_ctr = 0x00;
}

auto Accumulator::incr_reseed_ctr() noexcept -> void {
	std::lock_guard<std::mutex> lg(mtx);
	{ ++this->reseed_ctr; }
}

auto Accumulator::get_reseed_ctr() const noexcept -> uint64_t {
	std::lock_guard<std::mutex> lg(mtx);
	return this->reseed_ctr;
}

auto Accumulator::_p_pools_equal(
	std::shared_ptr<std::array<accumulator::Pool, Accumulator::NUM_OF_POOLS>>
		p_pools) const -> bool {
	if (this->_p_pools == p_pools) {
		return true;
	}
	return false;
}

// check if given source id is an id of an already registered entropy source
auto Accumulator::src_is_registered(const uint8_t& id) noexcept -> bool {
	std::atomic<bool> reg{false};
	static uint8_t _src_id{};

	if (std::any_of(this->entropy_sources.begin(),
					this->entropy_sources.end(),
					[&](auto& src_id) { return src_id == id; })) {
		reg = true;
		_src_id = id;
	}

	if (reg) {
		fmt::print(
			stderr,
			"[!] acc(add_source): entropy source \"{}\" already registered",
			_src_id);
		return true;
	}

	return false;
}

auto Accumulator::set_pools_ptr(
	std::shared_ptr<std::array<accumulator::Pool, Accumulator::NUM_OF_POOLS>>
		p_pools) noexcept -> void {
	this->_p_pools = p_pools;
}

auto Accumulator::set_gen_ptr(fortuna::generator::Generator& gen) -> void {
	this->Gen = &gen;
}

auto Accumulator::get_random_data(const unsigned int& n_bytes) -> std::string {
	std::string data{""};
	try {
		data = this->Gen->generate_random_data(n_bytes);
	}
	catch (std::exception& e) {
		// FIXME: handle the exception
		throw;
	}
	return data;
}

auto Accumulator::call_reseed(const std::string& seed) -> void {
	try {
		incr_reseed_ctr();
		this->Gen->reseed(seed);
	}
	catch (std::exception& e) {
		// FIXME: handle the exception
		throw;
	}
}

} // namespace accumulator
} // namespace fortuna

#endif // FORTUNA_ACCUMULATOR_CPP
