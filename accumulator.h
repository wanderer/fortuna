#ifndef FORTUNA_ACCUMULATOR_H
#define FORTUNA_ACCUMULATOR_H

#include "event_adder_impl.h"
#include "generator.h"
#include "pool.h"

#include <fmt/core.h>

#include <algorithm>
#include <array>
#include <cstdint>
#include <exception>
#include <memory>
#include <mutex>
#include <vector>

namespace fortuna {
namespace accumulator {

class Accumulator {
private:
	static constexpr const uint8_t MAX_SOURCES{255};
	static constexpr const uint8_t NUM_OF_POOLS{32};

	mutable std::mutex mtx; // used in const fun
	uint64_t reseed_ctr{0x00};

	std::vector<uint8_t> entropy_sources{};
	fortuna::generator::Generator* Gen;
	std::shared_ptr<std::array<accumulator::Pool, Accumulator::NUM_OF_POOLS>>
		_p_pools;

protected:
	unsigned int src_count{0};

public:
	constexpr static const unsigned int init_pool_num{0};

	auto set_reseed_ctr_to_null() noexcept -> void;

	auto incr_reseed_ctr() noexcept -> void;

	auto get_reseed_ctr() const noexcept -> uint64_t;

	auto _p_pools_equal(
		std::shared_ptr<std::array<accumulator::Pool,
								   Accumulator::NUM_OF_POOLS>> p_pools) const
		-> bool;

	[[maybe_unused]] auto add_source() -> void {
		static unsigned int src_id{this->src_count};
		// make really sure we don't add a duplicate src_id
		if (src_id <= this->MAX_SOURCES &&
			!src_is_registered(static_cast<uint8_t>(src_id))) {
			try {
				entropy_sources.push_back(static_cast<uint8_t>(src_id));
				++src_count;
				EventAdderImpl event_adder(src_id, this->_p_pools);
				[[maybe_unused]] bool scheduled;
			}
			catch (std::exception& e) {
				fmt::print(stderr, "{}\n", e.what());
			}
		}
	}

	[[maybe_unused]] auto src_is_registered(const uint8_t& id) noexcept -> bool;

	auto set_pools_ptr(std::shared_ptr<
					   std::array<accumulator::Pool, Accumulator::NUM_OF_POOLS>>
						   p_pools) noexcept -> void;

	auto set_gen_ptr(fortuna::generator::Generator& Gen) -> void;

	auto get_random_data(const unsigned int& n_bytes) -> std::string;

	auto call_reseed(const std::string& seed) -> void;

	auto wait_for(const unsigned int& milliseconds) -> void;

	// a long lived thread collecting entropy
	// listens on a unix socket, receives events
	auto entropy_collector_service() -> int;


	Accumulator() noexcept;
	~Accumulator() noexcept;
	Accumulator(const Accumulator&) = delete; // no copy
	Accumulator(Accumulator&) = delete;
	Accumulator& operator=(const Accumulator&) = delete;

}; // class Accumulator

} // namespace accumulator
} // namespace fortuna

#endif // FORTUNA_ACCUMULATOR_H
