#ifndef FORTUNA_UTIL_H
#define FORTUNA_UTIL_H

#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha3.h>

#include <array>
#include <chrono>
#include <cstddef>
#include <vector>

namespace fortuna {

class Util final {
public:
	static constexpr const std::size_t gen_block_size{32}; // 256 bits

	static auto de_hex(const std::string& str) -> const std::string {
		std::string s{str};
		std::string dst;
		CryptoPP::HexDecoder decoder;

		decoder.Put(reinterpret_cast<CryptoPP::byte*>(s.data()), s.size());
		decoder.MessageEnd();
		dst.resize(gen_block_size);
		decoder.Get(reinterpret_cast<CryptoPP::byte*>(&dst[0]), dst.size());

		return dst;
	}

	static auto do_sha(const std::string& str_to_hash) -> const std::string {
		// do sha256
		std::string digest;

		// no reason not to go for Keccak
		CryptoPP::SHA3_256 sha3_256;

		CryptoPP::StringSource str_src(
			str_to_hash,
			true,
			new CryptoPP::HashFilter(
				sha3_256,
				new CryptoPP::HexEncoder(new CryptoPP::StringSink(digest),
										 true))); // unify to uppercase

		return digest;
	}

	static auto do_sha(const std::vector<char>& char_vec) -> const std::string {
		std::string output;

		{
			CryptoPP::SHA3_256 sha3_256;
			CryptoPP::byte digest[CryptoPP::SHA3_256::DIGESTSIZE];

			{
				sha3_256.CalculateDigest(
					digest,
					reinterpret_cast<const CryptoPP::byte*>(&char_vec[0]),
					char_vec.size());
			}

			CryptoPP::HexEncoder encoder;
			encoder.Attach(new CryptoPP::StringSink(output));
			encoder.Put(digest, sizeof(digest));
			encoder.MessageEnd();
		}

		return output;
	}

	static auto do_sha(std::vector<char>& char_vec) -> const std::string {
		std::string output;

		{
			CryptoPP::SHA3_256 sha3_256;
			// convert vector to string
			const std::string s{std::string(char_vec.begin(), char_vec.end())};

			CryptoPP::StringSource src(
				s,
				true,
				new CryptoPP::HashFilter(
					sha3_256,
					new CryptoPP::HexEncoder(new CryptoPP::StringSink(output),
											 true)));
		}

		return output;
	}

	static auto current_time() -> std::chrono::time_point<
		std::chrono::system_clock,
		std::chrono::duration<long, std::ratio<1, 1000000000>>> {
		return std::chrono::system_clock::now();
	}

	// returns number of blocks for a given number of bytes
	static constexpr std::size_t b2b(std::size_t bytes) noexcept {
		// returns number of blocks
		return bytes == 0 ? 0 : ((bytes - 1) / gen_block_size) + 1;
	}

	Util() = delete;
	~Util() noexcept;

}; // class Util

} // namespace fortuna

#endif // FORTUNA_UTIL_H
